export class SelfRegisteredUserModel {
  public userId: string;
  public registrationType: string;
  public name: string;
  public contactNo: string;
  public nic: string;
  public userName: any;
  public centerName: string;
  public startTime: string;
  public address: string;
  public latitude: number;
  public longitude: number;
  public slmcNo: number;
  public qualification: string;
  public registeredDate: string;
}
