export class CenterUpdate {
  public id: number;
  public centerName: string;
  public contactNo: string;
  public status: string;
  public openTime: any;
  public mobitelAgent: string;
  public address: string;
  public city: string;
  public latitude: number;
  public longitude: number;
}
