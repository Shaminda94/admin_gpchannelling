
export class UserUpdate {
  public email: string;
  public firstName: string;
  public lastName: string;
  public contactNo: string;
  public status: string;
}
