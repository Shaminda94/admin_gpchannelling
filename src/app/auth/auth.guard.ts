import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from '../pages/login/login.service';



@Injectable()
export class AuthGuard implements CanActivate {

  base_url: string;

  constructor(private router: Router
    , private authService: LoginService) { }

  canActivate() {
    // Check to see if a user has a valid token
    if (localStorage.getItem('access_token')) {
      // If they do, return true and allow the user to load app
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }



  }


}