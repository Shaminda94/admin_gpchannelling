import { Component, OnInit } from '@angular/core';
import {UserCenter} from '../../../environments/user-center';
import {CenterUpdate} from '../../models/center-update.model';
import {CenterRegistrationService} from '../center/center.service';
import {Router} from '@angular/router';
import {SelfRegisteredUserModel} from '../../models/self-registered-user.model';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent implements OnInit {

  public selfRegisteredUserList: Array<SelfRegisteredUserModel> = [];
  public selfRegisteredUsers: SelfRegisteredUserModel[] = [];

  selfRegisteredUserModel = new SelfRegisteredUserModel();

  submitted = false;
  showSuccessMsg: boolean = false;
  showErrorMsg: boolean = false;
  message: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {

    this.userService.getSelfRegisteredUsers().then((res: SelfRegisteredUserModel[]) => {
      this.selfRegisteredUsers = res;
      for (let i = 0; i < this.selfRegisteredUsers.length; i++) {
        this.selfRegisteredUserList.push(this.selfRegisteredUsers[i]);
      }
    });
  }

  onSubmit() {
    this.submitted = true;

    this.userService.processSelfRegisteredUser(this.selfRegisteredUserModel.userId).then(
      data => {
        this.showSuccessMsg = true;
        this.message = 'Update Success!';
        this.selfRegisteredUserModel = new SelfRegisteredUserModel();
        this.selfRegisteredUserList = [];
        this.userService.getSelfRegisteredUsers().then((res: SelfRegisteredUserModel[]) => {
          this.selfRegisteredUsers = res;
          for (let i = 0; i < this.selfRegisteredUsers.length; i++) {
            this.selfRegisteredUserList.push(this.selfRegisteredUsers[i]);
          }
        });
        setTimeout(() => {this.showSuccessMsg = false }, 2000);
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;
        this.selfRegisteredUserModel = new SelfRegisteredUserModel();
        setTimeout(() => {this.showErrorMsg = false}, 2000);
      });
  }

  onChange(value: any) {
    for (let i=0; i < this.selfRegisteredUserList.length; i++){
      console.log(value);
      if (this.selfRegisteredUserList[i].userId == value.substring(3)) {
        this.selfRegisteredUserModel.registrationType = this.selfRegisteredUserList[i].registrationType;
        this.selfRegisteredUserModel.name = this.selfRegisteredUserList[i].name;
        this.selfRegisteredUserModel.nic = this.selfRegisteredUserList[i].nic;
        this.selfRegisteredUserModel.contactNo = this.selfRegisteredUserList[i].contactNo;
        this.selfRegisteredUserModel.userName = this.selfRegisteredUserList[i].userName;
        this.selfRegisteredUserModel.centerName = this.selfRegisteredUserList[i].centerName;
        this.selfRegisteredUserModel.startTime = this.selfRegisteredUserList[i].startTime;
        this.selfRegisteredUserModel.address = this.selfRegisteredUserList[i].address;
        this.selfRegisteredUserModel.latitude = this.selfRegisteredUserList[i].latitude;
        this.selfRegisteredUserModel.longitude = this.selfRegisteredUserList[i].longitude;
        this.selfRegisteredUserModel.slmcNo = this.selfRegisteredUserList[i].slmcNo;
        this.selfRegisteredUserModel.qualification = this.selfRegisteredUserList[i].qualification;

      }
    }

  }
}
