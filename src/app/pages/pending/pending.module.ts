import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './pending.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

/* components */
import { SelectModule } from 'ng2-select';
import {PendingComponent} from './pending.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing,
    SelectModule,
  ],
  declarations: [
    PendingComponent,

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PendingModule { }
