import { Routes, RouterModule } from '@angular/router';
import {PendingComponent} from './pending.component';

const childRoutes: Routes = [
  {
    path: '',
    component: PendingComponent,
    /*children: [
      // { path: '', redirectTo: 'levels1', pathMatch: 'full' },
      // { path: 'levels1', loadChildren: './components/levels-1/levels-1.module#Levels1Module' }
      {path: 'add-doctor' , component:AddDoctorComponent},
      { path: 'center-registration', component: CenterRegistrationComponent },
      { path: 'center-update', component: CenterUpdateComponent },
    ]*/
  }
];

export const routing = RouterModule.forChild(childRoutes);
