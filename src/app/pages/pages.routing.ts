import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../auth/auth.guard';

export const childRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'pages',
        canActivate: [AuthGuard],
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'index', pathMatch: 'full' },
            { path: 'index', loadChildren: './index/index.module#IndexModule' },
            { path: 'center', loadChildren: './center/center.module#CenterModule' },
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'pending', loadChildren: './pending/pending.module#PendingModule' }
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
