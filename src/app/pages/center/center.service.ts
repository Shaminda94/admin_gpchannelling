import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Center } from '../../../environments/center';
import { catchError } from 'rxjs/operators';
import { Type } from '@angular/compiler';
import { throwError } from 'rxjs';
import { CenterDoctor } from '../../../environments/center-doctor';
import {UserUpdate} from '../../models/user-update.model';
import {CenterUpdate} from '../../models/center-update.model';
import {User} from '../../../environments/user';
import {HttpService} from '../../services/http.service';

@Injectable({ providedIn: 'root', })
export class CenterRegistrationService {

  constructor(private _http: HttpClient, private httpService: HttpService) { }

  /*register(center: Center) {
    return this._http.post<any>(this._url + "centers", center, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
      .pipe(catchError(this.errorHandler))
  }*/
  register(center: Center) {
    return new Promise((resolve, reject) => {
      this.httpService.httpPost('/centers', center, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }

  /*getCenters() {
    return this._http.get(this._url + "users/centers", {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
  }*/
  getCenters() {
    return new Promise((resolve, reject) => {
      this.httpService.httpGet('/users/centers', {}, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }

  /*updateCenter(centerUpdate: CenterUpdate) {
    return this._http.put<any>(this._url + "centers/update", centerUpdate, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
      .pipe(catchError(this.errorHandler))
  }*/
  updateCenter(centerUpdate: CenterUpdate) {
    return new Promise((resolve, reject) => {
      this.httpService.httpPut('/centers/update', centerUpdate, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }

  /*getDoctors() {
    return this._http.get(this._url + "doctors/names", {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
  }*/
  getDoctors() {
    return new Promise((resolve, reject) => {
      this.httpService.httpGet('/doctors/names', {}, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }

  /*addDoctors(centerDoctor: CenterDoctor) {
    return this._http.put<any>(this._url + "centers/" + centerDoctor.centerId + "/doctors", centerDoctor, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
      .pipe(catchError(this.errorHandler))
  }*/
  addDoctors(centerDoctor: CenterDoctor) {
    return new Promise((resolve, reject) => {
      this.httpService.httpPost('/centers/' + centerDoctor.centerId + '/doctors', centerDoctor, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);

  }
}
