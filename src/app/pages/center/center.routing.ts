import { Routes, RouterModule } from '@angular/router';
import { CenterComponent } from './center.component';
import { CenterRegistrationComponent } from './components/center-registration/center-registration.component';
import { AddDoctorComponent } from './components/add-doctors/add-doctor.component';
import {CenterUpdateComponent} from './components/center-update/center-update.component';

const childRoutes: Routes = [
    {
        path: '',
        component: CenterComponent,
        children: [
            // { path: '', redirectTo: 'levels1', pathMatch: 'full' },
            // { path: 'levels1', loadChildren: './components/levels-1/levels-1.module#Levels1Module' }
            {path: 'add-doctor' , component:AddDoctorComponent},
            { path: 'center-registration', component: CenterRegistrationComponent },
            { path: 'center-update', component: CenterUpdateComponent },
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
