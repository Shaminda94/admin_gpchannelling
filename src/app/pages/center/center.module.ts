import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './center.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

/* components */
import { CenterComponent } from './center.component';
import { CenterRegistrationComponent } from './components/center-registration/center-registration.component';
import { AddDoctorComponent } from './components/add-doctors/add-doctor.component';
import { SelectModule } from 'ng2-select';
import { HttpClient } from '@angular/common/http';
import { CenterUpdateComponent } from './components/center-update/center-update.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        SelectModule,
    ],
    declarations: [
        CenterComponent,
        CenterRegistrationComponent,
        AddDoctorComponent,
        CenterUpdateComponent,

    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class CenterModule { }
