import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserCenter } from '../../../../../environments/user-center';
import { CenterRegistrationService } from '../../center.service';
import { DoctorCenter } from '../../../../../environments/doctor-center';
import { CenterDoctor } from '../../../../../environments/center-doctor';

@Component({
    selector: 'add-doctor',
    templateUrl: './add-doctor.component.html',
    styleUrls: []
})

export class AddDoctorComponent implements OnInit {

    public items1: Array<UserCenter> = [];
    public centers: UserCenter[] = []
    centerModel: UserCenter = new UserCenter();

    public items: Array<DoctorCenter> = [];
    public doctors: DoctorCenter[] = []
    doctorModel: DoctorCenter = new DoctorCenter();
    doctorModels: Array<DoctorCenter> = new Array<DoctorCenter>();

    centerDoctorModel = new CenterDoctor();
    selectedDoctor = this.doctorModel;
    submitted = false;
    showSuccessMsg: boolean = false;
    showErrorMsg: boolean = false;
    message: string;


    constructor(private centerService: CenterRegistrationService) { }
    ngOnInit() {

        this.centerService.getCenters().then((res: UserCenter[]) => {
            this.centers = res;
            for (var i = 0; i < this.centers.length; i++) {
                this.items1.push(this.centers[i]);
            }

        });

        this.centerService.getDoctors().then((res: DoctorCenter[]) => {
            this.doctors = res;
            for (var i = 0; i < this.doctors.length; i++) {
                this.items.push(this.doctors[i]);
            }

        });


    }
    addDoctor() {
        this.doctorModels.push(this.selectedDoctor);
        console.log(this.selectedDoctor)
        this.doctorModel = new DoctorCenter();
    }

    onSubmit() {
        this.submitted = true;
        this.centerDoctorModel.doctors = this.doctorModels;
        console.log(this.centerDoctorModel);
        this.centerService.addDoctors(this.centerDoctorModel).then(
            data => {
                this.showSuccessMsg = true;
                this.message = "Registration Success!";

            },
            error => {
                this.showErrorMsg = true;
                this.message = error.error.message;

            }
        )



    }

}



