import { Component, OnInit } from '@angular/core';
import { Center } from '../../../../../environments/center'
import { CenterRegistrationService } from '../../center.service'
import { Address } from '../../../../../environments/address';



@Component({
  selector: 'center-registration',
  templateUrl: './center-registration.component.html'
})

export class CenterRegistrationComponent implements OnInit {
  addressModel = new Address();
  centerModel = new Center(this.addressModel);
  // formHasError = true;
  submitted = false;

  showSuccessMsg: boolean = false;
  showErrorMsg: boolean = false;
  message: string;

  constructor(private _centerRegistration: CenterRegistrationService) { }

  ngOnInit() {
  }
  // validateForm(value) {
  //   if (value === 'default') {
  //     this.formHasError = true;
  //   } else {
  //     this.formHasError = false;
  //   }
  // }

  onSubmit() {
    this.submitted = true;
    this._centerRegistration.register(this.centerModel).then(
      data => {
        this.showSuccessMsg = true;
        this.message = "Registration Success!";
        setTimeout(() => {this.showSuccessMsg = false }, 2000);
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;
        setTimeout(() => {this.showErrorMsg = false }, 2000);
      });




  }

}
