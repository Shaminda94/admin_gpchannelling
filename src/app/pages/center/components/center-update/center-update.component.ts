import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserCenter} from '../../../../../environments/user-center';
import {CenterUpdate} from '../../../../models/center-update.model';
import {CenterRegistrationService} from '../../center.service';

@Component({
  selector: 'app-center-update',
  templateUrl: './center-update.component.html',
  styleUrls: ['./center-update.component.scss']
})
export class CenterUpdateComponent implements OnInit {

  public centerList: Array<UserCenter> = [];
  public centers: UserCenter[] = [];
  centerUpdateModel = new CenterUpdate();

  submitted = false;
  showSuccessMsg: boolean = false;
  showErrorMsg: boolean = false;
  message: string;

  constructor(private centerService: CenterRegistrationService, private router: Router) { }

  ngOnInit() {

    this.centerService.getCenters().then((res: UserCenter[]) => {
      this.centers = res;
      for (var i = 0; i < this.centers.length; i++) {
        this.centerList.push(this.centers[i]);
      }
    });
  }

  onSubmit() {
    this.submitted = true;

    this.centerService.updateCenter(this.centerUpdateModel).then(
      data => {
        this.showSuccessMsg = true;
        this.message = "Update Success!";
        this.centerUpdateModel = new CenterUpdate();
        this.centerList = [];
        this.centerService.getCenters().then((res: UserCenter[]) => {
          this.centers = res;
          for (var i = 0; i < this.centers.length; i++) {
            this.centerList.push(this.centers[i]);
          }
        });
        setTimeout(() => {this.showSuccessMsg = false }, 2000);
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;
        this.centerUpdateModel = new CenterUpdate();
        setTimeout(() => {this.showErrorMsg = false }, 2000);
      });
  }

  onChange(value: any) {
    for (let i=0; i < this.centerList.length; i++){
      if (this.centerList[i].centerId == value.substring(3)) {
        this.centerUpdateModel.centerName = this.centerList[i].centerName;
        this.centerUpdateModel.address = this.centerList[i].address.address;
        this.centerUpdateModel.city = this.centerList[i].address.city;
        this.centerUpdateModel.contactNo = this.centerList[i].contactNo;
        this.centerUpdateModel.latitude = this.centerList[i].address.latitude;
        this.centerUpdateModel.longitude = this.centerList[i].address.longitude;
        this.centerUpdateModel.openTime = this.centerList[i].openTime;
        this.centerUpdateModel.mobitelAgent = this.centerList[i].mobitelAgent;
        this.centerUpdateModel.status = this.centerList[i].status;
      }
    }

  }
}
