export let MENU_ITEM = [
    {
        path: 'index',
        title: 'Dashboard',
        icon: 'dashboard'
    },

    {
        path: 'center',
        title: 'Center',
        icon: 'sitemap',
        children: [

            {
                path: 'center-registration',
                title: 'Center Registration'
            },
          {
            path: 'center-update',
            title: 'Center Update'
          },
            {
                path: 'add-doctor',
                title: 'Add Doctor'
            },
        ]
    },{
        path: 'user',
        title: 'User',
        icon: 'sitemap',
        children: [

            {
                path: 'user-registration',
                title: 'User Registration'
            },
          {
            path: 'user-update',
            title: 'User Update'
          },
            {
                path: 'doctor-registration',
                title: 'Doctor Registration'
            }

        ]
    },{
    path: 'pending',
    title: 'Pending',
    icon: 'sitemap',
/*
    children: [

      {
        path: 'user-registration',
        title: 'User Registration'
      },
      {
        path: 'user-update',
        title: 'User Update'
      },
      {
        path: 'doctor-registration',
        title: 'Doctor Registration'
      }

    ]
*/
  }
];
