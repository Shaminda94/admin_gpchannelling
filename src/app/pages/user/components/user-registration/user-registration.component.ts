import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { Role } from '../../../../../environments/role';
import { User } from '../../../../../environments/user';
import { UserCenter } from '../../../../../environments/user-center';
import * as jsSHA from 'jssha';
import { Router } from "@angular/router";





@Component({
  selector: 'user-registration',
  templateUrl: './user-registration.component.html'
  //    styleUrls: ['./center-registration.component.scss']
})

export class UserRegistrationComponent implements OnInit {

  public items: Array<Role> = [];
  public roles: Role[] = []
  roleModel: Role = new Role();
  roleModels: Array<Role> = new Array<Role>();

  public items1: Array<UserCenter> = [];
  public centers: UserCenter[] = []
  centerModel: UserCenter = new UserCenter();
  centerModels: Array<UserCenter> = new Array<UserCenter>();


  selectedCenter = this.centerModel;

  selectedRole = this.roleModel;
  submitted = false;
  userModel = new User();
  showSuccessMsg: boolean = false;
  showErrorMsg: boolean = false;
  message: string;

  constructor(private userService: UserService, private router: Router) { }


  ngOnInit() {
    this.userService.getRoles().then((res: Role[]) => {
      this.roles = res;
      for (var i = 0; i < this.roles.length; i++) {
        this.items.push(this.roles[i]);
      }

    });


    this.userService.getCenters().then((res: UserCenter[]) => {
      this.centers = res;
      for (var i = 0; i < this.centers.length; i++) {
        this.items1.push(this.centers[i]);
      }

    });
  }


  addRoles() {
    this.roleModels.push(this.selectedRole);
    this.roleModel = new Role();
  }
  addCenter() {
    this.centerModels.push(this.selectedCenter);
    this.centerModel = new UserCenter();
  }

  createHash(hashText) {
    let js_sha = new jsSHA('SHA-256', 'TEXT');
    js_sha.update("ech@nne!-gpc" + btoa(hashText));
    const hasCode = js_sha.getHash('HEX');
    // console.log(hasCode);
    return hasCode.toUpperCase();
  }


  onSubmit() {
    this.submitted = true;

    this.userModel.centers = this.centerModels;
    this.userModel.roles = this.roleModels;

    //password encryption
    this.userModel.password = this.createHash(this.userModel.password);
    this.userModel.confirmPassword = this.createHash(this.userModel.confirmPassword);

    this.userService.register(this.userModel).then(
      data => {
        this.showSuccessMsg = true;
        this.message = "Registration Success!";
        this.userModel = new User();
        setTimeout(() => {this.showSuccessMsg = false }, 2000);
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;
        this.userModel = new User();
        setTimeout(() => {this.showErrorMsg = false }, 2000);
      });


  }
}
