import { Component, OnInit } from '@angular/core';
import {UserCenter} from '../../../../../environments/user-center';
import {User} from '../../../../../environments/user';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {UserUpdate} from '../../../../models/user-update.model';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {

  public userList: Array<User> = [];
  public users: User[] = [];
  userUpdateModel = new UserUpdate();

  submitted = false;
  showSuccessMsg: boolean = false;
  showErrorMsg: boolean = false;
  message: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {

    this.userService.getUsers().then((res: User[]) => {
      this.users = res;
      for (var i = 0; i < this.users.length; i++) {
        this.userList.push(this.users[i]);
      }
    });
  }

  onSubmit() {
    this.submitted = true;

    this.userService.updateUser(this.userUpdateModel).then(
      data => {
        this.showSuccessMsg = true;
        this.message = "Update Success!";
        this.userUpdateModel = new UserUpdate();
        this.userList = [];
        this.userService.getUsers().then((res: User[]) => {
          this.users = res;
          for (var i = 0; i < this.users.length; i++) {
            this.userList.push(this.users[i]);
          }
        });
        setTimeout(() => {this.showSuccessMsg = false }, 2000);
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;
        this.userUpdateModel = new UserUpdate();
        setTimeout(() => {this.showErrorMsg = false }, 2000);
      });
  }

  onChange(value: any) {
    for (let i=0; i < this.userList.length; i++){
      if (this.userList[i].email == value.substring(3).trim()) {
        this.userUpdateModel.firstName = this.userList[i].firstName;
        this.userUpdateModel.lastName = this.userList[i].lastName;
        this.userUpdateModel.contactNo = this.userList[i].contactNo;
        this.userUpdateModel.status = this.userList[i].status;

      }
    }
  }
}
