import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { User } from '../../../../../environments/user';
import { Doctor } from '../../../../../environments/doctor';
import { Qualification } from '../../../../../environments/qualification';


@Component({
    selector: 'doctor-registration',
    templateUrl: './doctor-registration.component.html',
    styleUrls: []
})

export class DoctorRegistrationComponent implements OnInit {
    submitted = false;
    public items1: Array<User> = [];
    public users: User[] = []
    userModel: User = new User;
    qualificationModel: Qualification = new Qualification;
    doctorModel: Doctor = new Doctor(this.userModel, this.qualificationModel);
    showSuccessMsg: boolean = false;
    showErrorMsg: boolean = false;
    message: string;


    constructor(private userService: UserService) { }
    ngOnInit() {

        this.userService.getUsers().then((res: User[]) => {
            this.users = res;
            for (var i = 0; i < this.users.length; i++) {
                this.items1.push(this.users[i]);
            }

        });


    }

    onSubmit() {

        this.submitted = true;

        this.userService.registerDoctor(this.doctorModel).then(
            data => {
                this.showSuccessMsg = true;
                this.message = "Registration Success!";


            },
            error => {
                this.showErrorMsg = true;
                this.message = error.error.message;

            });


    }
}

