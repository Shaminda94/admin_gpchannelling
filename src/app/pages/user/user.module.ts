import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './user.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


/* components */
import { UserComponent } from './user.component';
import {UserRegistrationComponent } from './components/user-registration/user-registration.component';
import { SelectModule } from 'ng2-select';
import { ConfirmEqualValidatorDirective } from '../../shared/confirm-equal-validator.directive';
import { DoctorRegistrationComponent } from './components/doctor-registration/doctor-registration.component';
import { from } from 'rxjs';
import { UserUpdateComponent } from './components/user-update/user-update.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        SelectModule,
     

       
    ],
    declarations: [
        UserComponent,
        DoctorRegistrationComponent,
        UserRegistrationComponent,
        ConfirmEqualValidatorDirective,
        UserUpdateComponent,

    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class UserModule { }
