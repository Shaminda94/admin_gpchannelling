import { Injectable } from "@angular/core";
import { User } from '../../../environments/user';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Doctor } from '../../../environments/doctor';
import {UserUpdate} from '../../models/user-update.model';
import {HttpService} from '../../services/http.service';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private httpClient: HttpClient, private httpService: HttpService) { }

   /* getRoles() {
        return this.httpClient.get(this._url + "roles", {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}});
    }*/

    getRoles() {
      return new Promise((resolve, reject) => {
        this.httpService.httpGet('/roles', {}, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }

   /* getCenters() {
        return this.httpClient.get(this._url + "users/centers", {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
    }*/

    getCenters() {
      return new Promise((resolve, reject) => {
        this.httpService.httpGet('/users/centers', {}, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }

    /*getUsers(){
        return this.httpClient.get(this._url + "users", {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
    }*/

    getUsers() {
      return new Promise((resolve, reject) => {
        this.httpService.httpGet('/users', {}, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }

   /* register(user: User) {
        return this.httpClient.post<any>(this._url + "users/register", user, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
            .pipe(catchError(this.errorHandler));
    }*/

    register(user: User) {
      return new Promise((resolve, reject) => {
        this.httpService.httpPost('/users/register', user, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }
    /*updateUser(userUpdate: UserUpdate) {
      return this.httpClient.put<any>(this._url + "users/update", userUpdate, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
        .pipe(catchError(this.errorHandler))
    }*/

    updateUser(userUpdate: UserUpdate) {
      return new Promise((resolve, reject) => {
        this.httpService.httpPut('/users/update', userUpdate, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }
    /*registerDoctor(doctor: Doctor) {
        return this.httpClient.post<any>(this._url + "doctors/register", doctor, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
            .pipe(catchError(this.errorHandler))
    }*/

    registerDoctor(doctor: Doctor) {
      return new Promise((resolve, reject) => {
        this.httpService.httpPost('/doctors/register', doctor, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }
    errorHandler(error: HttpErrorResponse) {
        return throwError(error);

    }

  getSelfRegisteredUsers() {
    return new Promise((resolve, reject) => {
      this.httpService.httpGet('/self-registered-users', {}, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }

  processSelfRegisteredUser(userId: string) {
    return new Promise((resolve, reject) => {
      this.httpService.httpPut('/self-registered-users/' + userId, {}, {})
        .then((response: any) => {
          resolve(response);
        }).catch((error: any) => {
        resolve(error);
      });
    });
  }
}
