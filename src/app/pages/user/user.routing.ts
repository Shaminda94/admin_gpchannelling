import { Routes, RouterModule } from '@angular/router';
import {UserComponent } from './user.component';
import { UserRegistrationComponent } from './components/user-registration/user-registration.component';
import { DoctorRegistrationComponent } from './components/doctor-registration/doctor-registration.component';
import {UserUpdateComponent} from './components/user-update/user-update.component';


const childRoutes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            // { path: '', redirectTo: 'levels1', pathMatch: 'full' },
            // { path: 'levels1', loadChildren: './components/levels-1/levels-1.module#Levels1Module' }
             {path: 'doctor-registration' , component:DoctorRegistrationComponent},
            { path: 'user-registration', component: UserRegistrationComponent },
            { path: 'user-update', component: UserUpdateComponent },
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
