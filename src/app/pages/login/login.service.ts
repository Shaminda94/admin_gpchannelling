import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Center } from '../../../environments/center';
import { catchError } from 'rxjs/operators';
import { Type } from '@angular/compiler';
import { throwError } from 'rxjs';
import { CenterDoctor } from '../../../environments/center-doctor';
import { User } from '../../../environments/user';
import {HttpService} from '../../services/http.service';

@Injectable({ providedIn: 'root', })
export class LoginService {
    private loggedInStatus = false;

  constructor(private _http: HttpClient, private httpService: HttpService) {}

    /*getUserDetails(user: User) {
        return this._http.post<any>(this._url + "users/signin", user, {headers: {'x-ibm-client-id': 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21'}})
            .pipe(catchError(this.errorHandler));
    }*/

    getUserDetails(user: User) {
      return new Promise((resolve, reject) => {
        this.httpService.httpPost('/users/signin', user, {})
          .then((response: any) => {
            resolve(response);
          }).catch((error: any) => {
          resolve(error);
        });
      });
    }

    errorHandler(error: HttpErrorResponse) {
        return throwError(error);

    }

    setLoggedIn(value: boolean) {
        this.loggedInStatus = value;
        localStorage.setItem('access_token', "true");
    }

    getLoggedIn(){
        return this.loggedInStatus;
    }

}
