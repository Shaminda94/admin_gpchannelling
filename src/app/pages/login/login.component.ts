import { Component, OnInit } from '@angular/core';
import { User } from '../../../environments/user';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

import * as jsSHA from 'jssha';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  userModel = new User();
  submitted = false;
  showErrorMsg: boolean = false;
  message: string

  constructor(private auth: LoginService,
    private router: Router) {

  }

  ngOnInit() { }

  createHash(hashText) {
    let js_sha = new jsSHA('SHA-256', 'TEXT');
    js_sha.update("ech@nne!-gpc" + btoa(hashText));
    const hasCode = js_sha.getHash('HEX');
    // console.log(hasCode);
    return hasCode.toUpperCase();
  }


  onSubmit() {
    this.submitted = true;
    this.userModel.password = this.createHash(this.userModel.password)
    this.auth.getUserDetails(this.userModel).then(
      data => {
        let tempData: any;
        tempData = data;
        this.auth.setLoggedIn(true);
        this.userModel = tempData.data;

        if (this.userModel.userType == 'ECHADMIN') {
          this.router.navigate(['pages']);
        } else {
          this.showErrorMsg = true;
          this.message = "Access Denied";
        }
      },
      error => {
        this.showErrorMsg = true;
        this.message = error.error.message;

      });




  }

}



