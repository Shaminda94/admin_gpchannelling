import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { stringify } from 'querystring';

@Injectable()
export class HttpService {

  constructor(private httpClient: HttpClient) { }

  private url = 'https://apphub.mobitel.lk/mobitelint/mapis/gpc-service';
 // private url = 'http://localhost:8080/gpc-service';
 // private url = 'https://172.26.144.34:8443/gpc-service';

  private static setHeader (header: any, headerValue: any) {
    if (headerValue) {
      for (const key in headerValue) {
        header = header.append(key, headerValue[key]);
      }
    }
    return header;
  }

  private static httpErrorHandler(error) {
    switch (error.status) {
      case 304:
        const eTag = error.headers.get('Etag');
        if (eTag) {
          error.errorMessage = eTag.replace(/"/g, '');
        }
        break;
      case 401:
        error.errorMessage = 'User not authorized';
        break;
      case 500:
        error.errorMessage = 'System Error';
        break;
      default:
        break;
    }
    return error;
  }

  public httpGet(path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
      let header = new HttpHeaders().set('Content-Type', 'application/json');
      header = header.append('Accept', '*/*');
      header = header.append('x-ibm-client-id', 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21');

      const httpHeaders = HttpService.setHeader(header, headerValue);

      const url = this.url + path;
      return this.httpClient.get(url, { headers: httpHeaders} )
        .toPromise()
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(HttpService.httpErrorHandler(error));
        });
    });
  }

  public httpPost(path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
      const req_body: string = JSON.stringify(body);
      let header = new HttpHeaders().set('Content-Type', 'application/json');
      header = header.append('Accept', '*/*');

      header = header.append('x-ibm-client-id', 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21');

      const httpHeaders = HttpService.setHeader(header, headerValue);

      const url = this.url + path;

      return this.httpClient.request('POST', url, {body: req_body, headers: httpHeaders, observe: 'response'})
        .toPromise()
        .then(response => {
          const responseData: any = [];
          responseData.data = response.body;
          responseData.status = response.status;

          resolve(responseData);
        })
        .catch(error => {
          reject(HttpService.httpErrorHandler(error));
        });
    });
  }

  public httpPut(path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
      const req_body = JSON.stringify(body);
      let header = new HttpHeaders().set('Content-Type', 'application/json');
      header = header.append('Accept', '*/*');
      header = header.append('x-ibm-client-id', 'bbbbaa68-beaf-4ac3-979b-5d726aba1e21');

      const httpHeaders = HttpService.setHeader(header, headerValue);

      const url = this.url + path;

      return this.httpClient.request('PUT', url, {body: req_body, headers: httpHeaders, observe: 'response'})
        .toPromise()
        .then(response => {

          const responseData: any = [];
          responseData.data = response.body;
          responseData.status = response.status;

          resolve(responseData);
        })
        .catch(error => {
          reject(HttpService.httpErrorHandler(error));
        });
    });
  }


  public httpGetLocalJson(path: string) {
    return new Promise( (resolve, reject) => {
      const header = new HttpHeaders().set('Content-Type', 'application/json');
      const httpHeaders = HttpService.setHeader(header, {});
      const url = path;
      return this.httpClient.get(url, {headers: httpHeaders})
        .toPromise()
        .then( (response: any) => {
          resolve(response);
        });
    });
  }

}
