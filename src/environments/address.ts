export class Address {
    constructor() { }
    public city: string;
    public latitude: number;
    public longitude: number;
    public address: string;

}
