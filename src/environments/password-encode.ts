//import { jsSHA } from 'angular-sha';
import * as jsSHA from 'jssha';

export class PasswordEncode{
    public createHash(hashText) {
        let js_sha = new jsSHA('SHA-256', 'TEXT');
        js_sha.update("" + btoa(hashText));
        const hasCode = js_sha.getHash('HEX');
        // console.log(hasCode);
        return hasCode.toUpperCase();
    }
}