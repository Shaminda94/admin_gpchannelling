import { Role } from './role';
import { UserCenter } from './user-center';

export class User {
    public userId: number;
    public firstName: string;
    public lastName: string;
    public roles: Role[];
    public firstLoginStatus: string;
    public password: string;
    public confirmPassword: string;
    public email: string;
    public contactNo: string;
    public userType: string;
    public nic: string;
    public status: string;
    public centers: UserCenter[];
}
